SRC=src
TARGET=bin
INC=include
CC=g++ -O2 -pipe -Wall -DDEBUG=0

all: $(TARGET)/spl

$(TARGET)/spl: $(SRC)/lex.yy.c $(SRC)/spl.tab.h $(SRC)/spl.tab.c $(TARGET)/engine.o $(TARGET)/interpreter.o $(TARGET)/ast.o $(TARGET)/spl_exception.o
	$(CC) $^ -lfl -o $@

$(SRC)/lex.yy.c: $(SRC)/spl.l $(SRC)/spl.tab.h
	flex -Ca $(SRC)/spl.l
	mv lex.yy.c $(SRC)

$(SRC)/spl.tab.h $(SRC)/spl.tab.c: $(SRC)/spl.y
	bison $(SRC)/spl.y -b $(SRC)/spl --defines=$(SRC)/spl.tab.h

$(TARGET)/engine.o: $(SRC)/engine.cpp $(SRC)/engine.h
	$(CC) -c $< -o $@

$(TARGET)/interpreter.o: $(SRC)/interpreter.cpp $(SRC)/interpreter.h
	$(CC) -c $< -o $@

$(TARGET)/ast.o: $(SRC)/ast.cpp $(SRC)/ast.h
	$(CC) -c $< -o $@

$(TARGET)/spl_exception.o: $(SRC)/spl_exception.cpp $(SRC)/spl_exception.h
	$(CC) -c $< -o $@

$(SRC)/spl.l: $(INC)/negative_adjective.metaflex $(INC)/positive_adjective.metaflex $(INC)/character.wordlist $(INC)/char.metaflex $(INC)/negative_adjective.wordlist $(INC)/negative_comparative.wordlist $(INC)/negative_comp.metaflex $(INC)/negative_noun.metaflex $(INC)/negative_noun.wordlist $(INC)/neutral_adjective.wordlist $(INC)/neutral_noun.wordlist $(INC)/positive_adjective.wordlist $(INC)/positive_comparative.wordlist $(INC)/positive_comparative.metaflex $(INC)/positive_noun.metaflex $(INC)/positive_noun.wordlist $(INC)/user_code_bottom.metaflex $(INC)/user_code_top.metaflex
	$(INC)/make.sh $(SRC)/spl.l

clean:
	rm -f $(SRC)/spl.l $(SRC)/spl.tab.h $(SRC)/spl.tab.c $(SRC)/lex.yy.c $(TARGET)/*
