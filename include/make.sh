#!/bin/sh

INC=$(dirname $0)
OUT=$1

cat $INC/user_code_top.metaflex > $OUT
cat $INC/character.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e '$!s/$/|/'| tr -d '\n' >> $OUT
cat $INC/char.metaflex >> $OUT
cat $INC/positive_noun.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e 's/$/|/'| tr -d '\n' >> $OUT
cat $INC/neutral_noun.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e '$!s/$/|/'| tr -d '\n' >> $OUT
cat $INC/positive_noun.metaflex >> $OUT
cat $INC/negative_noun.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e '$!s/$/|/'| tr -d '\n' >> $OUT
cat $INC/negative_noun.metaflex >> $OUT
cat $INC/positive_adjective.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e 's/$/|/'| tr -d '\n' >> $OUT
cat $INC/neutral_adjective.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e '$!s/$/|/'| tr -d '\n' >> $OUT
cat $INC/positive_adjective.metaflex >> $OUT
cat $INC/negative_adjective.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e '$!s/$/|/'| tr -d '\n' >> $OUT
cat $INC/negative_adjective.metaflex >> $OUT
cat $INC/positive_comparative.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e '$!s/$/|/'| tr -d '\n' >> $OUT
cat $INC/positive_comparative.metaflex >> $OUT
cat $INC/negative_comparative.wordlist| sed -e 's/ /"[[:space:]]+"/g' -e 's/^/"/g' -e 's/$/"/g' -e '$!s/$/|/'| tr -d '\n' >> $OUT
cat $INC/negative_comp.metaflex >> $OUT
cat $INC/user_code_bottom.metaflex >> $OUT
