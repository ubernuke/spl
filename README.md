spl
===

Interpréteur en C/C++ pour le Shakespeare Programming Language.

Nécessite g++, flex et bison.

Compilation:
> make

Lancement du programme:
> bin/spl FILE
