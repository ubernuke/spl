#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

using namespace std;

#ifndef AST_H
#define AST_H

enum astType{var, you, me, number, assignment, binary, unary, test, conditional};
enum opType{opFactorial, opSquare, opCube, opSquareroot, opCubicroot, opBigger, opLower, opEqual, opIf, opIfnot};

struct ast
{
    enum astType type;
    union
    {
		int val;
		char *name;
        struct
        {
            struct ast *left, *right;
            char op;
	    bool negation;
        } binary;
        struct
        {
            struct ast *right;
            enum opType op;
        } unary;
    } data;
};

struct ast* makeNumber(int val);
struct ast* makeVar(char *name);
struct ast* makeYou();
struct ast* makeMe();
struct ast* makeBinary(struct ast *left, struct ast *right, char op);
struct ast* makeUnary(struct ast *right, enum opType op);
struct ast* makeGotoAct(char *n);
struct ast* makeGotoScene(char *n);
struct ast* makeUnary(struct ast *right, enum opType op);
struct ast* makeTest(bool negation, struct ast *left, struct ast *right, char op);
void printAst(struct ast *tree, int level);
void freeAst(struct ast *tree);
#endif
