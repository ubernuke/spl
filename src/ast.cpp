#include "ast.h"


void freeAst(struct ast *tree)
{
	if(tree->type == binary || tree->type == test) {
		freeAst(tree->data.binary.left);
		freeAst(tree->data.binary.right);
	}
	else if(tree->type == unary || tree->type == conditional) {
		freeAst(tree->data.unary.right);
	}
	else if(tree->type == var)
		free(tree->data.name);

	free(tree);
}

struct ast* makeNumber(int val)
{
    struct ast *result = (struct ast*)malloc(sizeof(struct ast));
	result->type = number;
	result->data.val = val;
    return result;
}

struct ast* makeVar(char *name)
{
    struct ast *result = (struct ast*)malloc(sizeof(struct ast));
	result->type = var;
	result->data.name = strdup(name);
    return result;
}

struct ast* makeYou()
{
    struct ast *result = (struct ast*)malloc(sizeof(struct ast));
	result->type = you;
    return result;
}

struct ast* makeMe()
{
    struct ast *result = (struct ast*)malloc(sizeof(struct ast));
	result->type = me;
    return result;
}

struct ast* makeBinary(struct ast *left, struct ast *right, char op)
{
    struct ast *result = (struct ast*)malloc(sizeof(struct ast));
	result->type = binary;
	result->data.binary.op = op;
	result->data.binary.left = left;
	result->data.binary.right = right;
    return result;
}

struct ast* makeUnary(struct ast *right, enum opType op)
{
    struct ast *result = (struct ast*)malloc(sizeof(struct ast));
	result->type = unary;
	result->data.unary.op = op;
	result->data.unary.right = right;
    return result;
}

struct ast* makeTest(bool negation, struct ast *left, struct ast *right, char op)
{
    struct ast *result = (struct ast*)malloc(sizeof(struct ast));
	result->type = test;
	result->data.binary.op = op;
	result->data.binary.left = left;
	result->data.binary.right = right;
	result->data.binary.negation = negation;
    return result;
}

struct ast* makeConditional(struct ast *right, enum opType op)
{
    struct ast *result = (struct ast*)malloc(sizeof(struct ast));
	result->type = unary;
	result->data.unary.op = op;
	result->data.unary.right = right;
    return result;
}

void printAst(struct ast *tree, int level)
{
	for(int i=0; i<level; ++i)
		cout << "  ";
	switch(tree->type)
	{
		case var:
			cout << tree->data.name << endl;
			break;
		case me:
			cout << "me" << endl;
			break;
		case you:
			cout << "you" << endl;
			break;
		case number:
			cout << tree->data.val << endl;
			break;
		case binary:
			cout << tree->data.binary.op << endl;
			printAst(tree->data.binary.left, level+1);
			printAst(tree->data.binary.right, level+1);
			break;
		case test:
			cout << tree->data.binary.op << endl;
			printAst(tree->data.binary.left, level+1);
			printAst(tree->data.binary.right, level+1);
			break;
		case unary:
			switch(tree->data.unary.op)
			{
				case opSquare:
					cout << "^2" << endl;
					break;
				case opCube:
					cout << "^3" << endl;
					break;
				case opFactorial:
					cout << "!" << endl;
					break;
				case opSquareroot:
					cout << "square root" << endl;
					break;
				case opCubicroot:
					cout << "cubic root" << endl;
					break;
				case opLower:
					cout << "<" << endl;
					break;
				case opBigger:
					cout << ">" << endl;
					break;
				case opIf:
					cout << "if" << endl;
					break;
				case opIfnot:
					cout << "if not" << endl;
					break;
				default:
					break;
			}
			printAst(tree->data.unary.right, level+1);
			break;
		default:
			break;
	}
}
