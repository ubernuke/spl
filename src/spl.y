%error-verbose
%{
#include <cstdio>
#include <iostream>
#include <unistd.h>
using namespace std;

#include "spl.tab.h"  // to get the token types that we return
#include "interpreter.h"

// stuff from flex that bison needs to know about:
extern "C" int yylex();
#if YYPARSE_PARAM
extern "C" int yyparse();
#endif
extern "C" FILE *yyin;
extern int yylineno;
 
void yyerror(const char *s);

//create engine
interpreter inter;
%}

// Bison fundamentally works by asking flex to get the next token, which it
// returns as an object of type "yystype".  But tokens could be of any
// arbitrary data type!  So we deal with that in Bison by defining a C union
// holding each of the types of tokens that Flex could return, and have Bison
// use that union instead of "int" for the definition of "yystype":
%union {
	long int ival;
//	float fval;
	char *sval;
	struct ast *tree;
	bool bval;
}

// define the constant-string tokens:
%token ACT SCENE ENTER EXIT EXEUNT REMEMBER RECALL
%token YOU THOU ARE ART THE ARTICLE YOURSELF MYSELF NOTHING AS NOT IS AM_I MORE LESS
%token NEG_ADJ POS_ADJ BAD_NOUN GOOD_NOUN POSITIVE_COMPAR NEGATIVE_COMPAR THAN IF_SO IF_NOT LET_US GOTO
%token PRINTINT PRINTCHAR READINT READCHAR TWICE SUM_OF DIFFERENCE_BETWEEN PRODUCT_OF QUOTIENT_BETWEEN SQUARE_OF CUBE_OF SQUARE_ROOT_OF CUBIC_ROOT_OF REMAINDER FACTORIAL_OF
%token OPEN CLOSE
%token COMMA SEMICOLON COLON PERIOD EXCL INTER
%left AND


// define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the union:
%token <sval> STRING CHARACTER ROMAN

%type<tree> simple_value adjectives noun expression value
%type<bval> negation

%%

play:
	title init script
	;
title:
     comment
	;
init:
	init declaration
    | declaration
	;
declaration:
    CHARACTER COMMA words PERIOD { inter.eng.addCharacter($1); free($1); }
    ;
script:
    act script
    | act
    ;
act:
    ACT ROMAN { inter.beginAct($2); free($2); } COLON comment scenes
    ;
scenes:
	scene scenes
	| scene
	;
scene:
     SCENE ROMAN { inter.beginScene($2); free($2); } COLON comment dialog
     ;
dialog:
     line dialog
     | line
     ;
line:
    direction
	| reply
    ;
direction:
    OPEN ENTER characters CLOSE { inter.enterList(); }
    | OPEN EXIT CHARACTER CLOSE { inter.exit($3); free($3); }
    | OPEN EXEUNT CLOSE { inter.exitAll(); }
    | OPEN EXEUNT CHARACTER AND characters CLOSE { inter.addName($3); free($3); inter.exitList(); }
    ;
characters:
	characters AND CHARACTER { inter.addName($3); free($3); }
	| CHARACTER { inter.addName($1); free($1); }
	; 
reply:
    CHARACTER COLON { inter.setTalker($1); free($1); } statements
	;
statements:
	statements statement
	| statement
	;
statement:
	affirmation
	| question
	| answer
	;
affirmation:
	PRINTINT DOT { inter.printInt(); }
	| PRINTCHAR DOT { inter.printChar(); }
	| READINT DOT { inter.readInt(); }
	| READCHAR DOT { inter.readChar(); }
	| YOU simple_value EXCL { inter.assign($2); }
	| YOU ARE value DOT { inter.assign($3); }
	| THOU simple_value EXCL { inter.assign($2); }
	| THOU ART value DOT { inter.assign($3); }
	| YOU ARE AS adjective AS value DOT { inter.assign($6); }
	| THOU ART AS adjective AS value DOT { inter.assign($6); }
	| goto
	| REMEMBER value { inter.remember($2); } DOT
	| RECALL words { inter.recall(); } DOT
	;
goto:
	LET_US GOTO ACT ROMAN DOT { inter.gotoAct($4); free($4); }
	| LET_US GOTO SCENE ROMAN DOT { inter.gotoScene($4); free($4); }
	;
question:
	are_you negation AS adjective AS value INTER { inter.askQuestion(makeTest($2, makeYou(), $6, '=')); }
	| are_you negation MORE POS_ADJ THAN value INTER { inter.askQuestion(makeTest($2, makeYou(), $6, '>')); }
	| are_you negation LESS POS_ADJ THAN value INTER { inter.askQuestion(makeTest($2, makeYou(), $6, '<')); }
	| are_you negation MORE NEG_ADJ THAN value INTER { inter.askQuestion(makeTest($2, makeYou(), $6, '<')); }
	| are_you negation LESS NEG_ADJ THAN value INTER { inter.askQuestion(makeTest($2, makeYou(), $6, '>')); }
	| are_you negation POSITIVE_COMPAR THAN value INTER { inter.askQuestion(makeTest($2, makeYou(), $5, '>')); }
	| are_you negation NEGATIVE_COMPAR THAN value INTER { inter.askQuestion(makeTest($2, makeYou(), $5, '<')); }
	| AM_I negation AS adjective AS value INTER { inter.askQuestion(makeTest($2, makeMe(), $6, '=')); }
	| AM_I negation POSITIVE_COMPAR THAN value INTER { inter.askQuestion(makeTest($2, makeMe(), $5, '>')); }
	| AM_I negation NEGATIVE_COMPAR THAN value INTER { inter.askQuestion(makeTest($2, makeMe(), $5, '<')); }
	| IS value negation AS adjective AS value INTER { inter.askQuestion(makeTest($3, $2, $7, '=')); }
	| IS value negation POSITIVE_COMPAR THAN value INTER { inter.askQuestion(makeTest($3, $2, $6, '>')); }
	| IS value negation NEGATIVE_COMPAR THAN value INTER { inter.askQuestion(makeTest($3, $2, $6, '<')); }
	;
negation:
	NOT { $$ = false; }
	| { $$ = true; }
	;
are_you:
	ARE YOU
	| ART THOU
	;
answer:
	IF_SO { inter.answer(true); } COMMA affirmation 
	| IF_NOT { inter.answer(false); } COMMA affirmation
	;
value:
	expression { $$ = $1; }
	| THE expression { $$ = $2; }
	| ARTICLE expression { $$ = $2; }
	| THE simple_value { $$ = $2; }
	| ARTICLE simple_value { $$ = $2; }
	;
expression:
	CHARACTER { $$ = makeVar($1); free($1); }
	| YOURSELF { $$ = makeYou(); }
	| YOU { $$ = makeYou(); }
	| MYSELF { $$ = makeMe(); }
	| THE SUM_OF value AND value { $$ = makeBinary($3, $5, '+'); }
	| THE DIFFERENCE_BETWEEN value AND value { $$ = makeBinary($3, $5, '-'); }
	| THE PRODUCT_OF value AND value { $$ = makeBinary($3, $5, '*'); }
	| THE FACTORIAL_OF value { $$ = makeUnary($3, opFactorial); }
	| THE QUOTIENT_BETWEEN value AND value { $$ = makeBinary($3, $5, '/'); }
	| THE REMAINDER value AND value { $$ = makeBinary($3, $5, '%'); }
	| TWICE value { $$ = makeBinary(makeNumber(2), $2, '*'); }
	| THE SQUARE_OF value { $$ = makeUnary($3, opSquare); }
	| THE CUBE_OF value { $$ = makeUnary($3, opCube); }
	| THE SQUARE_ROOT_OF value { $$ = makeUnary($3, opSquareroot); }
	| THE CUBIC_ROOT_OF value { $$ = makeUnary($3, opCubicroot); }
	| NOTHING { $$ = makeNumber(0); }
	;
simple_value:
	adjectives noun { $$ = makeBinary($1, $2, '*'); }
	| noun { $$ = $1; }
	;
adjectives:
	adjective adjectives { $$ = makeBinary(makeNumber(2), $2, '*'); }
	| adjective { $$ = makeNumber(2); }
	;
adjective:
	POS_ADJ | NEG_ADJ
	;
noun:
	GOOD_NOUN { $$ = makeNumber(1); } | BAD_NOUN { $$ = makeNumber(-1); }
	;
comment:
    words DOT
    ;
words:
    words word
    | word
    ;
word:
	STRING { free($1); } | CHARACTER { free($1); } | ROMAN { free($1); }
	| ACT | SCENE | ENTER | EXIT | EXEUNT | REMEMBER | RECALL
	| YOU | THOU | ARE | ART | THE | ARTICLE | YOURSELF | MYSELF | NOTHING | AS | NOT | IS | AM_I | LESS | MORE
	| adjective | BAD_NOUN | GOOD_NOUN | POSITIVE_COMPAR | NEGATIVE_COMPAR | THAN | IF_SO | IF_NOT | LET_US | GOTO
	| PRINTINT | PRINTCHAR | READINT | READCHAR | TWICE | SUM_OF | DIFFERENCE_BETWEEN | PRODUCT_OF | QUOTIENT_BETWEEN | SQUARE_OF | CUBE_OF | SQUARE_ROOT_OF | CUBIC_ROOT_OF | REMAINDER | FACTORIAL_OF
	| OPEN | CLOSE
	| COLON
	| AND
	| PUNCT
	;
PUNCT:
    COMMA
    | SEMICOLON
    ;
DOT:
	PERIOD
	| EXCL
	;
%%

int main(int argc, char *argv[]) {

	int res = 0;
	int c;

	opterr = 0;
	while ((c = getopt (argc, argv, "hv")) != -1) {
		switch (c)
		{
			case 'v':
				cout << "SPL interpreter version 1.0" << endl;
				return 0;
			case 'h':
				cout << "Usage: spl [OPTIONS|FILE]\n  -h                        Print this help\n  -v                        Show version number" << endl;
				return 0;
			case '?':
				if (isprint (optopt))
				  fprintf (stderr, "Unknown option `-%c'.\n", optopt);
				else
				  fprintf (stderr,
					   "Unknown option character `\\x%x'.\n",
					   optopt);
				return 1;
			default:
				break;
		}
	}

	if(argc > 1) {
		// open a file handle to a particular file:
		FILE *myfile = fopen(argv[1], "r");
		// make sure it's valid:
		if (!myfile) {
			cerr << "Can't open file " << argv[1] << endl;
			return -1;
		}
		// set flex to read from it instead of defaulting to STDIN:
		yyin = myfile;
	}

	try
	{
		// parse through the input until there is no more:
		do {
			yyparse();
		} while (!feof(yyin));
#if DEBUG
		inter.printStack();
#endif
		inter.execute();
	}
	catch (const std::exception &e) 
	{
		std::cerr << e.what() << endl; 
		res = 1;
	}

	if(argc > 1)
		fclose(yyin);
#if DEBUG
	if(res == 0)
		cout << "Done" << endl; 
#endif
	return res;
}

void yyerror(const char *s) {
	cerr << "Error line " << yylineno << ": " << s << endl;
	// might as well halt now:
	exit(1);
}
