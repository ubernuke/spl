#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>
#include <math.h>
#include <map>
#include <vector>
#include <queue>
#include "ast.h"
#include "engine.h"

#ifndef INTERPRETER_H
#define INTERPRETER_H

using namespace std;

enum insType{insTalker, insEnter, insExit, insExitall, insPrintint, insPrintchar, insReadint, insReadchar, insAssignment, insGotoAct, insGotoScene, insAsk, insCond, insRemember, insRecall};

struct instruction
{
	enum insType type;
	unsigned int line;

	union
	{
		char *name;
		struct ast *expression;
		bool val;
	} data;
};

class interpreter
{
	private:
		unsigned int lineNb, actNb, sceneNb;
		vector<struct instruction> stack;
		map<string, unsigned int> labels;
		queue<string> namelist;
		void executeAssignment(struct instruction *ins);
		void executeTest(struct instruction *ins);
		void executeInstruction(struct instruction *ins);
		int getTreeValue(struct ast *tree);
		bool lastTest, skipNext;
		void freeStack();
		void freeInstruction(struct instruction *ins);
	public:
		engine eng;
		interpreter();
		~interpreter();
		void setLine(unsigned int line);
		void beginAct(string roman);
		void beginScene(string roman);
		void gotoAct(string n);
		void gotoScene(string n);
		void askQuestion(struct ast *tree);
		void answer(bool val);
		void setTalker(string name);
		void printInt();
		void printChar();
		void readInt();
		void readChar();
		void enter(string name);
		void enterList();
		void exit(string name);
		void exitList();
		void exitAll();
		void addName(string name);
		void assign(struct ast *tree);
		void remember(struct ast *tree);
		void recall();
		void printStack();
		void execute();
};

#endif
