#include "interpreter.h"

using namespace std;

interpreter::interpreter(void)
{
	this->lineNb = 1;
	this->actNb = 0;
	this->sceneNb = 0;
	this->skipNext = false;
}

interpreter::~interpreter()
{
	freeStack();
}

void interpreter::setLine(unsigned int line)
{
	this->lineNb = line;
}

unsigned int romanToInt(string s)
{
    unsigned int sum=0,i=0;
    while(s[i]!='\0')
    {
        switch(s[i])
        {
            case 'M':sum+=1000;
                 break;
            case 'D':if(i>0 && s[i-1]=='C')
                    sum+=400;
                 else if(i>0 && s[i-1]=='L')
                        sum+=450;
                 else
                    sum+=500;
                 break;
            case 'C':if(s[i+1]!='D')
                 {
                    if(i>0 && s[i-1]=='L')
                        sum+=50;
                    else
                        sum+=100;
                 }
                 break;
            case 'L':if(s[i+1]!='C')
                    sum+=50;
                 break;
            case 'X':if(i>0 && s[i-1]=='I')
                    sum+=9;
                 else
                    sum+=10;
                 break;
            case 'V':if(i>0 && s[i-1]=='I')
                    sum+=4;
                 else
                    sum+=5;
                 break;
            case 'I':if(s[i+1]!='X' && s[i+1]!='V')
                    sum+=1;
                 break;
            default : return -1;
        }
        ++i;
    }
    return sum;
}

void interpreter::beginAct(string roman)
{
	unsigned int val = romanToInt(roman);

	if(val < 0)
		eng.error("Invalid roman number");
	if(val != this->actNb+1)
		eng.error("Wrong act number");
	
	++actNb;
	sceneNb = 0;
	this->labels["act"+roman] = this->stack.size();
#if DEBUG
	cout << "Beginning act " << val << endl;
	cout << "Adding act" << roman << " with val " << this->labels["act"+roman] << endl;
#endif
}

void interpreter::beginScene(string roman)
{
	unsigned int val = romanToInt(roman);

	if(val < 0)
		eng.error("Invalid roman number");
	if(val != this->sceneNb+1)
		eng.error("Wrong scene number");
	
#if DEBUG
	cout << "Beginning scene " << val << endl;
#endif
	++sceneNb;
	ostringstream oss;
	oss << "act" << this->actNb << "scene" << roman;
	string s = oss.str();
	this->labels[s] = this->stack.size();
}

void interpreter::gotoAct(string n)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insGotoAct;
	ostringstream oss;
	oss << "act" << n;
	string s = oss.str();
	ins.data.name = strdup(s.c_str());
	this->stack.push_back(ins);
}

void interpreter::gotoScene(string n)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insGotoScene;
	ostringstream oss;
	oss << "act" << this->actNb << "scene" << n;
	string s = oss.str();
	ins.data.name = strdup(s.c_str());
	this->stack.push_back(ins);
}

void interpreter::askQuestion(struct ast *tree)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insAsk;
	ins.data.expression = tree;
	this->stack.push_back(ins);
}

void interpreter::answer(bool val)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insCond;
	ins.data.val = val;
	this->stack.push_back(ins);
}

void interpreter::setTalker(string name)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insTalker;
	ins.data.name = strdup(name.c_str());
	this->stack.push_back(ins);
}

void interpreter::printInt()
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insPrintint;
	this->stack.push_back(ins);
}

void interpreter::printChar()
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insPrintchar;
	this->stack.push_back(ins);
}

void interpreter::readInt()
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insReadint;
	this->stack.push_back(ins);
}

void interpreter::readChar()
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insReadchar;
	this->stack.push_back(ins);
}

void interpreter::addName(string name)
{
    namelist.push(name);
}

void interpreter::enter(string name)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insEnter;
	ins.data.name = strdup(name.c_str());
	this->stack.push_back(ins);
}

void interpreter::enterList()
{
    for(size_t i = 0, size = this->namelist.size(); i < size; ++i)
	{
		enter(this->namelist.front());
		this->namelist.pop();
	}
}

void interpreter::exit(string name)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insExit;
	ins.data.name = strdup(name.c_str());
	this->stack.push_back(ins);
}

void interpreter::exitList()
{
    for(size_t i = 0, size = this->namelist.size(); i < size; ++i)
	{
		interpreter::exit(this->namelist.front());
		this->namelist.pop();
	}
}

void interpreter::exitAll()
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insExitall;
	this->stack.push_back(ins);
}

void interpreter::assign(struct ast *tree)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insAssignment;
	ins.data.expression = tree;
	this->stack.push_back(ins);
}

void interpreter::remember(struct ast *tree)
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insRemember;
	ins.data.expression = tree;
	this->stack.push_back(ins);
}

void interpreter::recall()
{
	struct instruction ins;
	ins.line = this->lineNb;
	ins.type = insRecall;
	this->stack.push_back(ins);
}

int factorial(int n) {
	int i,res = 1;
	for(i=2; i<=n; i++) {
		res *= i;
	}

	return res;
}

int interpreter::getTreeValue(struct ast *tree)
{
	int left, right;
	switch(tree->type) {
		case(var):
			return this->eng.getVal(tree->data.name);
			break;
		case(you):
			return this->eng.getInterlocutorVal();
			break;
		case(me):
			return this->eng.getTalkerVal();
			break;
		case(number):
			return tree->data.val;
			break;
		case(binary):
			left = getTreeValue(tree->data.binary.left);
			right = getTreeValue(tree->data.binary.right);
			switch(tree->data.binary.op) {
				case '+':
					return (left + right);
					break;
				case '-':
					return (left - right);
					break;
				case '*':
					return (left * right);
					break;
				case '/':
					return (int)(left / right);
					break;
				case '%':
					return (left % right);
					break;
			}
			break;
		case(unary):
			right = getTreeValue(tree->data.unary.right);
			switch(tree->data.unary.op) {
				case opFactorial:
					return factorial(right);
					break;
				case opSquare:
					return (int)pow(right, 2);
					break;
				case opCube:
					return pow(right, 3);
					break;
				case opSquareroot:
					return (int)sqrt(right);
					break;
				case opCubicroot:
					return (int)cbrt(right);
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
	
	return 0;
}

void interpreter::executeAssignment(struct instruction *ins)
{
	int val = getTreeValue(ins->data.expression);
	eng.assignValue(val);
}

void interpreter::executeTest(struct instruction *ins)
{
	int val1 = getTreeValue((ins->data.expression)->data.binary.left);
	int val2 = getTreeValue((ins->data.expression)->data.binary.right);
	bool neg = (ins->data.expression)->data.binary.negation;
	bool res = false;

#if DEBUG
	cout << "Evaluating " << val1 << (ins->data.expression)->data.binary.op << val2 << endl;
#endif

	switch((ins->data.expression)->data.binary.op) {
		case('='):
			res = (val1 == val2);
			break;
		case('>'):
			res = (val1 > val2);
			break;
		case('<'):
			res = (val1 < val2);
			break;
	}

	this->lastTest = (neg == res);
}

void interpreter::executeInstruction(struct instruction *ins)
{
	eng.setLine(ins->line);
#if DEBUG
	cout << "line:" << ins->line << endl;
#endif

	if(ins->type == insAssignment) {
		if(!this->skipNext)
			executeAssignment(ins);
		else
			this->skipNext = false;
	}
	else if(ins->type == insAsk)
		executeTest(ins);
	else
	{
		switch(ins->type) {
			case(insTalker):
				this->eng.setTalker(ins->data.name);
				break;
			case(insEnter):
				this->eng.enter(ins->data.name);
				break;
			case(insExit):
				this->eng.exit(ins->data.name);
				break;
			case(insExitall):
				this->eng.exitAll();
				break;
			case(insPrintint):
				this->eng.printInt();
				break;
			case(insPrintchar):
				this->eng.printChar();
				break;
			case(insReadint):
				this->eng.readInt();
				break;
			case(insReadchar):
				this->eng.readChar();
				break;
			case(insRemember):
				this->eng.remember(getTreeValue(ins->data.expression));
				break;
			case(insRecall):
				this->eng.recall();
				break;
			default:
				return;
		}
	}
}

void interpreter::execute()
{
	struct instruction *ins;

#if DEBUG
	cout << "=== Execution ===" << endl;
#endif
	unsigned int i=0;
	while(i < this->stack.size()) {

		ins = &(this->stack[i]);
		if(!this->skipNext)
		{
			if(ins->type == insGotoAct) {
				if(this->labels.find(ins->data.name) == this->labels.end())
					eng.error("Unknow act number");

				i = this->labels[ins->data.name];
#if DEBUG
				cout << "Go to act " << ins->data.name << endl;
#endif
			}
			else if(ins->type == insGotoScene) {
				if(this->labels.find(ins->data.name) == this->labels.end())
					eng.error("Unknow scene number");

				i = this->labels[ins->data.name];
#if DEBUG
				cout << "Go to scene " << ins->data.name << endl;
#endif
			}
			else
			{
				executeInstruction(ins);
				++i;
			}
		}
		else
		{
			this->skipNext = false;
			++i;
		}

		if(ins->type == insCond) 
			this->skipNext = (ins->data.val != this->lastTest);
	}
}

void printInstruction(struct instruction *ins)
{
	cout << "line:" << ins->line << " ";
	switch(ins->type) {
		case(insTalker):
			cout << "talker " << ins->data.name << endl;
			break;
		case(insEnter):
			cout << "enter " << ins->data.name << endl;
			break;
		case(insExit):
			cout << "exit " << ins->data.name << endl;
			break;
		case(insExitall):
			cout << "exit all" << endl;
			break;
		case(insPrintint):
			cout << "print int" << endl;
			break;
		case(insPrintchar):
			cout << "print char" << endl;
			break;
		case(insReadint):
			cout << "read int" << endl;
			break;
		case(insReadchar):
			cout << "read char" << endl;
			break;
		case(insAssignment):
			cout << "assignment" << endl;
			printAst(ins->data.expression, 0);
			break;
		case(insGotoAct):
			cout << "goto act" << endl;
			break;
		case(insGotoScene):
			cout << "goto scene" << endl;
			break;
		case(insAsk):
			cout << "ask" << endl;
			printAst(ins->data.expression, 0);
			break;
		case(insCond):
			cout << "conditional " << (ins->data.val? "true": "false") << endl;
			break;
		case(insRemember):
			cout << "remember " << endl;
			printAst(ins->data.expression, 0);
			break;
		case(insRecall):
			cout << "recall" << endl;
			break;
	}
}

void interpreter::printStack()
{
	cout << "=== Stack ===" << endl;
	for(vector<struct instruction>::size_type i = 0; i != this->stack.size(); ++i) {
		printInstruction(&(this->stack[i]));
	}
}

void interpreter::freeStack()
{
	for(vector<struct instruction>::size_type i = 0; i != this->stack.size(); ++i) {
		freeInstruction(&(this->stack[i]));
	}
}

void interpreter::freeInstruction(struct instruction *ins)
{
	if(ins->type == insAsk || ins->type == insAssignment || ins->type == insRemember)
		freeAst(ins->data.expression);
	else if(ins->type == insTalker || ins->type == insEnter || ins->type == insExit || ins->type == insGotoAct || ins->type == insGotoScene)
		free(ins->data.name);
}
