#include <iostream> 
#include <sstream> 
#include <exception> 

#ifndef EXC_H
#define EXC_H

class spl_exception : public std::exception 
{ 
	public: 
		spl_exception( const char * Msg, int Line );
		virtual ~spl_exception() throw();
		virtual const char * what() const throw();

	private: 
		std::string msg; 
};

#endif
