#include "spl_exception.h"

using namespace std;

spl_exception::spl_exception( const char * Msg, int Line ) 
{ 
	ostringstream oss; 
	oss << "Error line " << Line << " : " << Msg; 
	this->msg = oss.str(); 
} 

spl_exception::~spl_exception() throw() 
{ 

} 

const char* spl_exception::what() const throw() 
{ 
	return this->msg.c_str(); 
} 
/*
int main() 
{ 
    try 
    { 
	std::string msg = "can't do this";
        throw spl_exception(msg.c_str(), __LINE__); 
    } 
    catch (const std::exception &e) 
    { 
        std::cerr << e.what() << "\n"; 
    } 
}
*/
