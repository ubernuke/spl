#include "engine.h"

using namespace std;


void lowercase(string *s)
{
	std::transform(s->begin(), s->end(), s->begin(), ::tolower);
}

engine::engine()
{
	this->lineNb = 1;
}

engine::~engine()
{
	for(std::map<string, character>::const_iterator iterator = this->characters.begin(); iterator != this->characters.end(); iterator++)
		freeCharacter(&(iterator->second));
}

void engine::error(string msg)
{
	throw spl_exception(msg.c_str(), lineNb);
}

void engine::setLine(unsigned int line)
{
    lineNb = line;
}

void engine::addCharacter(string name)
{
	lowercase(&name);
	if(characters.find(name) != characters.end())
		error("Character " + name + " has already been declared");

#if DEBUG
	cout << "Adding character " << name << endl;
#endif

	struct character c;
	makeCharacter(&c);
	characters[name] = c;
}

void engine::enter(string name)
{
	lowercase(&name);
	if(characters.find(name) == characters.end())
		error("Character " + name + " has not been declared");
	if(isOnStage(name))
		error("Character " + name + " is already on stage");

#if DEBUG
	cout << "Setting " << name << " on stage" << endl;
#endif
	stage.push_back(name);
}

void engine::exit(string name)
{
	lowercase(&name);
	if(characters.find(name) == characters.end())
		error("Character " + name + " has not been declared");
	if(!isOnStage(name))
		error("Character " + name + " is not on stage");

#if DEBUG
	cout << "Setting " << name << " off stage" << endl;
#endif

	for(std::vector<int>::size_type i = 0; i != stage.size(); i++) {
		if(stage[i] == name) {
			stage.erase(stage.begin()+i);
			break;
		}
	}
}

void engine::exitAll()
{
#if DEBUG
	cout << "Setting everyone off stage" << endl;
#endif
	stage.clear();
}

long int engine::getVal(string name)
{
	lowercase(&name);
	return characters[name].val;
}

long int engine::getInterlocutorVal()
{
	return characters[getInterlocutor()].val;
}

long int engine::getTalkerVal()
{
	if(stage.size() == 0)
		error("No character on stage");

	return characters[talker].val;
}

void engine::setTalker(string name)
{
	lowercase(&name);
	if(characters.find(name) == characters.end())
		error("Character " + name + " has not been declared");
	if(!isOnStage(name))
		error("Character " + name + " is not on stage");

	talker = name;
}

bool engine::isOnStage(string name)
{
	lowercase(&name);
	bool res = false;
	for(std::vector<int>::size_type i = 0; i != stage.size(); i++) {
		if(stage[i] == name)
			res = true;
	}

	return res;
}

void engine::assignValue(long int val)
{
	if(stage.size() < 2)
		error("No character to assign value to");
	if(stage.size() > 2)
		error("Ambiguous assignation, more than two characters on stage");
	
#if DEBUG
	cout << "Assigning value " << val << " to " << getInterlocutor() << endl;
#endif
	characters[getInterlocutor()].val = val;
}

string engine::getInterlocutor()
{
	if(stage.size() < 2)
		error("No other character is on stage");
	if(stage.size() > 2)
		error("Ambiguous command, more than two characters on stage");

	string res = "";
	for(std::vector<int>::size_type i = 0; i != stage.size(); i++) {
		if(stage[i] != talker) {
			res = stage[i];
			break;
		}
	}
	return res;
}

void engine::print(bool numeric)
{
	if(stage.size() < 2)
		error("No character to speak out");
	if(stage.size() > 2)
		error("Ambiguous command, more than two characters on stage");
	
	int val = characters[getInterlocutor()].val;

	if(numeric)
		cout << val;
	else
		cout << (char)val;
}

void engine::printChar()
{
	print(false);
}

void engine::printInt()
{
	print(true);
}

void engine::readInt()
{
	if(stage.size() < 2)
		error("No character to assign value to");

	int n;
	cin >> n;

	if(!cin)
		n = -1;
#if DEBUG
	cout << "Assigning value " << n << " to " << getInterlocutor() << endl;
#endif
	characters[getInterlocutor()].val = n;
}

void engine::readChar()
{
	if(stage.size() < 2)
		error("No character to assign value to");

	char c = getc(stdin);
	int n = c;
	if(c == EOF)
		n = -1;
	else
		getc(stdin);
#if DEBUG
	cout << "Assigning value " << n << " to " << getInterlocutor() << endl;
#endif
	characters[getInterlocutor()].val = n;
}

void engine::remember(long int val)
{
	this->characters[getInterlocutor()].s->push(val);
#if DEBUG
	cout << getInterlocutor() << " remembering value " << val << endl;
#endif
}

void engine::recall()
{
	string name = getInterlocutor();
	character *c = &(characters[name]);
	if(c->s->size() > 0)
	{
		c->val = c->s->top();
		c->s->pop();
#if DEBUG
		cout << name << " recalling value " << c->val << endl;
#endif
	}
	else
		error("Character "+name+" has no value to recall");
}

//character-related functions

void engine::makeCharacter(character *c)
{
	c->val = 0;
	c->s = new stack<int>();
}

void engine::freeCharacter(const character *c)
{
	delete c->s;
}
