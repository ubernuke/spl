#include <cstdlib>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <queue>
#include <stack>
#include <algorithm>
#include <stdio.h>

#include "spl_exception.h"

#ifndef ENG_H
#define ENG_H

using namespace std;

void error(string msg);


typedef struct character {
	long int val;
	stack<int>* s;
} character;

class engine
{
	public:
		unsigned int lineNb;
		engine();
		~engine();
		void setLine(unsigned int line);
		void addCharacter(string name);
		void enter(string name);
		void exit(string name);
		void exitAll();
		long int getVal(string name);
		long int getInterlocutorVal();
		long int getTalkerVal();
		void setTalker(string name);
		void assignValue(long int val);
		void printChar();
		void printInt();
		void readChar();
		void readInt();
		void remember(long int val);
		void recall();
		void error(string msg);
	private:
		unsigned int actNb, sceneNb;
		string talker;
		map<string, character> characters;
		vector<string> stage;
		bool lastTest;
		void print(bool numeric);
		bool isOnStage(string name);
		string getInterlocutor();
		void makeCharacter(character *c);
		void freeCharacter(const character *c);
};
#endif
